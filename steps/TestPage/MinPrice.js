const {When, Then} = require('cucumber');
const testpage = require('../../pageObject/testpage');

When("the user search a MIN price of cars", ()=> {
    testpage.MonthlyPrice.click();
    testpage.sliderMaxHandle2.dragAndDrop(testpage.slideMaxHandle1)
    testpage.saveBtn.click();
    browser.pause(2000);
    })

Then("results of related cars with MIN price are displayed", ()=>{
    if(testpage.resultOfMinText == '48 to choose from'){
        console.log(`Result of a 'Min price' serching is correct`)
    } else{
        console.log(`Consider an issue`);
     }  
})
